package com.moviePicker;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class is not required if I request for JSON response
 * @author Kavi
 *
 */

@Deprecated
public class OmdbXMLResponseParser {

	// No generics
	List<JSONObject> movieDataList;
	List domList;

	public OmdbXMLResponseParser() {
		// create a list to hold the MovieData objects
		movieDataList = new ArrayList<JSONObject>();
		domList = new ArrayList();
	}

	public void runExample() {

		// parse the xml file and get the dom object
		parseXmlFile();

		// get each MovieData element and create a MovieData object
		parseDocument();

		// Iterate through the list and print the data

		printData();

	}

	private void printData() {

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(
					"C:\\KAVI-PERSONAL\\MovieOrganizer\\src\\resources\\json\\IMDBResults.json");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("No of Movies '" + movieDataList.size() + "'.");

		Iterator it = movieDataList.iterator();
		while (it.hasNext()) {
			System.out.println();
			try {
				fos.write(((JSONObject) it.next()).toString().getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void parseXmlFile() {
		// get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			// Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			// parse using builder to get DOM representation of the XML file
			Document dom = db.parse(".//src//resources//movie.xml");
			domList.add(dom);
			dom = db.parse(".//src//resources//titanic.xml");
			domList.add(dom);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void parseDocument() {
		JSONObject jsonObj = null;
		// get the root elememt
		for (int i = 0; i < domList.size(); i++) {
			Element docEle = ((Document) domList.get(i)).getDocumentElement();

			NodeList nl = docEle.getElementsByTagName("movie");
			if (nl != null && nl.getLength() > 0) {
				for (int j = 0; j < nl.getLength(); j++) {

					// get the MovieData element
					Element el = (Element) nl.item(j);

					// get the MovieData object
					MovieDetailsResponseBean responseBean = getMovieData(el);

					jsonObj = new JSONObject(responseBean);
					// add it to list
					movieDataList.add(jsonObj);
				}
			}
		}
	}

	/**
	 * I take an MovieData element and read the values in, create an MovieData
	 * object and return it
	 * 
	 * @param empEl
	 * @return
	 */
	private MovieDetailsResponseBean getMovieData(Element elMD) {

		// Create a new MovieData with the value read from the xml nodes
		MovieDetailsResponseBean responseBean = new MovieDetailsResponseBean();
		responseBean.setActors(elMD.getAttribute("actors"));
		responseBean.setGenre(elMD.getAttribute("genre"));
		responseBean.setImdbRating(elMD.getAttribute("imdbRating"));
		responseBean.setPlot(elMD.getAttribute("plot"));
		responseBean.setRated(elMD.getAttribute("rated"));
		responseBean.setReleased(elMD.getAttribute("released"));
		responseBean.setTitle(elMD.getAttribute("title"));
		responseBean.setYear(elMD.getAttribute("year"));

		return responseBean;
	}

	/**
	 * I take a xml element and the tag name, look for the tag and get the text
	 * content i.e for <MovieData ><name>John</name></MovieData > xml snippet if
	 * the Element points to MovieData node and tagName is name I will return
	 * John
	 * 
	 * @param ele
	 * @param tagName
	 * @return
	 */

	private String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	public static void main(String[] args) {
		// create an instance
		OmdbXMLResponseParser dpe = new OmdbXMLResponseParser();

		// call run example
		dpe.runExample();
	}

}
