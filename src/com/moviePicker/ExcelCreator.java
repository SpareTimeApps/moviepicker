package com.moviePicker;

/**
 * Deprecated, if I use json response, I can stright away store it to load on an HTML.
 */

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

@Deprecated
public class ExcelCreator {

	private WritableCellFormat times;
	private String inputFile;
	
public void setOutputFile(String inputFile) {
	this.inputFile = inputFile;
	}

	public void populateExcelSheet() throws IOException, WriteException {
		File file = new File(inputFile);
		WorkbookSettings wbSettings = new WorkbookSettings();

		wbSettings.setLocale(new Locale("en", "EN"));

		WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
		workbook.createSheet("Report", 0);
		WritableSheet excelSheet = workbook.getSheet(0);
		createLabel(excelSheet);

		workbook.write();
		workbook.close();
	}

	private void createLabel(WritableSheet sheet)
			throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

	
		CellView cv = new CellView();
		cv.setFormat(times);

		// Write a few headers
		addCaption(sheet, 0, 0, "Title");
		addCaption(sheet, 1, 0, "Rating");
		addCaption(sheet, 2, 0, "Link to IMDB");
		addCaption(sheet, 3, 0, "Link to Movie in HD");
		createContent (sheet, 0,1, "testing..");

	}

	private void createContent(WritableSheet sheet, int column, int row, String s) throws WriteException,
			RowsExceededException {
		Label label;
		for (int i=0;i<10; i++){
		label = new Label(column, row, s, times);
		++row;
		sheet.addCell(label);
	}
	}

	private void addCaption(WritableSheet sheet, int column, int row, String s)
			throws RowsExceededException, WriteException {
		Label label;
		label = new Label(column, row, s, times);
		sheet.addCell(label);
	}


	public static void main(String[] args) throws WriteException, IOException {
		ExcelCreator test = new ExcelCreator();
		test.setOutputFile(".//src//test.xls");
		test.populateExcelSheet();
		System.out
				.println("Please check the result file under c:/temp/lars.xls ");
	}
}