package com.moviePicker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MovieNameUtil {

	public static void pathResolver() {

	}

	/**
	 * Typically the movies are organised in the HardDrive under
	 * (I:\Movies\English\MovieName\movie.avi/mp4 The movie file may have been
	 * downloaded from any source and may have some long string junk as the file
	 * name. But the folder name under which a movie is stored is named after
	 * the movie name.
	 * 
	 * Ex: Valid folder names for movies
	 * 
	 * fantastic four fantastic four (2015) fantastic four_(2005)
	 * 
	 * But the IMDB service, expects fantastic four for title, and 2015 in the
	 * year parameter. We don't want to give special characters.
	 * 
	 * 
	 * @param path
	 * @return
	 */
	 
	public static List getAllTitles(String path) {

		List<String> titleList = new ArrayList<String>();

		File root = new File(path);

		// Get the list of files and folders under the given root path.
		String[] movieFolders = root.list();

		for (int i = 0; i < movieFolders.length; i++) {
			File f = new File(root + "\\" + movieFolders[i]);

			// we only care if it is a folder, then it may have a movie in it,
			// that is same as the name of the folder.
			if (f.isDirectory()) {
				String year;
				String name = new File(movieFolders[i]).getName();

				// The name may also have year of release as part of the name.
				// If so, extract
				// the year into the year of release. attribute, and let the
				// name just have name without the year.
				String regex = "(\\d{4})";
				Matcher matcher = Pattern.compile(regex).matcher(name);

				while (matcher.find()) {
					String num = matcher.group();
					int yearIndex = name.indexOf(num);

					year = name.substring(yearIndex, yearIndex + 4);
					name = name.replace(num, "");
				}
				// remove all special characters
				String nameRefined = name.replaceAll("[^a-zA-Z]+", " ");
				System.out.println("Name:  " + name + "|| Refined Name: "
						+ nameRefined);
				// Usually the first few words from the movie name is enough to
				// call the WS, so don't bother if the title has 10 words, just
				// get the first 3
				StringTokenizer st = new StringTokenizer(nameRefined);
				int numOfTokens = st.countTokens();
				String title;

				if (numOfTokens < 3) {
					title = name;
				} else {
					title = st.nextToken() + " " + st.nextToken() + " "
							+ st.nextToken();
				}
				titleList.add(title);
			}
		}
		return titleList;
	}

}
