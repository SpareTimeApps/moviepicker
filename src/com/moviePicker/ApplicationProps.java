package com.moviePicker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**

 * 
 */
public class ApplicationProps {

	static private Properties props = new Properties();
	static InputStream input = null;
	private static final String root = "root";
	private static String omdbApiBaseURL = "omdbApiBaseURL";
	private static String responseType = "responseType";
	private static String waitInMilliSeconds= "waitInMilliSeconds";

	static {

		try {
			input = new FileInputStream("resources/config.properties");
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String getRoot() {
		return props.getProperty(root);
	}

	public static String getOmdbApiBaseURL() {
		return  props.getProperty(omdbApiBaseURL);
	}

	public static String getResponseType() {
		return  props.getProperty(responseType);
	}

	public static String getWaitInMilliSeconds() {
		return  props.getProperty(waitInMilliSeconds);
	}



}
