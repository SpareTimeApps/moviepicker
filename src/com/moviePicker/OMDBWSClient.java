package com.moviePicker;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class OMDBWSClient {

	public void getMovieInfoForEach(MovieDetailsRequestBean movieBean) {

		try {
			URL requestURL = buildNewRequest(movieBean);
			URLConnection yc = requestURL.openConnection();
			InputStream in = yc.getInputStream();
			if (in.available() <= 0) {
				throw new Exception("empty response.");
			}
			Path path = Paths.get(ApplicationProps.getRoot());

			if (!Files.exists(path)) {
				System.out.println("File does not exists");
				Files.createDirectories(path);

			}

			// TODO response file name should have year or release or any such
			// unique info so that movies released in the same name do not
			// overwrite
			// each other. For now let this be.
			path = path.resolve(movieBean.getTitle() + ".json");

			// TODO implement logging.
			System.out.println("response file in location: " + path);

			Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public URL buildNewRequest(MovieDetailsRequestBean movieBean) throws Exception {

		// make sure there is movie title to construct the WS request.
		if (movieBean.getTitle() == null) {
			throw new Exception("No title found, cannot build request.");
		}
		// request parameters for appending later.
		StringBuilder sbParameters = new StringBuilder();
		sbParameters.append("?t=" + movieBean.getTitle());

		// incase if there are additional attributes such as year of release or
		// type etc.
		if (movieBean.getYearOfRelease() != null && movieBean.getYearOfRelease().trim().length() > 0) {
			sbParameters.append("&year=" + movieBean.getYearOfRelease());
		}

		// specify "type" if its a movie, a series or episode
		if (movieBean.getType() != null && movieBean.getType().trim().length() > 0) {
			sbParameters.append("&type=" + movieBean.getType());
		}

		sbParameters.append("&r=" + ApplicationProps.getResponseType());
		// http://www.omdbapi.com
		URL url = new URL(ApplicationProps.getOmdbApiBaseURL() + sbParameters.toString());
		System.out.println(url);
		return url;

	}
}
